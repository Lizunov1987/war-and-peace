﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TestTask
{
    class Program
    {
        static IEnumerable<string> GetCleanArray(string path)
        {
            using var stream = new StreamReader(path, CodePagesEncodingProvider.Instance.GetEncoding(1251));
            StringBuilder sb = new StringBuilder();
            while (!stream.EndOfStream)
            {
                char ch = (char)stream.Read();
                if (char.IsLetter(ch))
                {
                    if (char.IsUpper(ch))
                        sb.Append(char.ToLower(ch));
                    else
                        sb.Append(ch);
                }
                else if(ch=='-')
                {
                    if (sb.Length > 0)
                        sb.Append(ch);
                }
                else if (char.IsWhiteSpace(ch) || char.IsPunctuation(ch))
                {
                    if (sb.Length > 0)
                    {
                        yield return sb.ToString();
                        sb.Clear();
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            Dictionary<string, List<string>> dict = new Dictionary<string, List<string>>();
            var arr = GetCleanArray("voyna-i-mir.txt").ToArray();

            for (int i = 0; i < arr.Length - 1; i++)
            {
                if (!dict.ContainsKey(arr[i]))
                    dict[arr[i]] = new List<string>() { arr[i + 1] };
                else
                    dict[arr[i]].Add(arr[i + 1]);
            }

            var top20 = dict.GroupBy(x => x).OrderByDescending(x => x.Key.Value.Count()).Take(20).ToArray();
            foreach (var item in top20)
            {
                var word = item.Key;
                var nextWords = item.Key.Value;
                Console.WriteLine(word.Key + " " + nextWords.Count);
                var top5 = nextWords.GroupBy(x => x, StringComparer.InvariantCultureIgnoreCase).OrderByDescending(x => x.Count()).Take(5);
                foreach (var top in top5)
                {
                    Console.WriteLine("\t" + top.Key + " " + top.Count());
                }
                Console.WriteLine();
            }
        }

    }


}
